'''
Run this Python3 script on your RPi like this:
python3 SensorDS.py test -v4 -nodb -port 45678 -dlist home/433mhzsensors/1

Then connect to the Device from your local host like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://rpi.lan:45678/home/433mhzsensors/1#dbase=no')

Subscribe to events (hardware updates every minute or so)
id = dp.subscribe_event('power_total_usage', tango.EventType.CHANGE_EVENT, tango.utils.EventCallback())

'''
import RFXtrx
import tango
from tango import (
    DevDouble,
    DevBoolean,
    DevState,
    DevVoid,
    EnsureOmniThread
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

DEVICE_COMMS='/dev/ttyUSB0'

class Rx433MHzSensors(Device):
    def init_device(self):
        super().init_device()

        # Instrument 'handle'
        self.__rfxcomm = None

        # initially we don't have a reading so set None?
        self.__meterreading = None
        self.__temperatures = {
            '01': None,
            '02': None,
            '03': None
        }
        for temp_channel in self.__temperatures:
            self.set_archive_event(f'temperature{temp_channel}', True, True)
            self.set_change_event(f'temperature{temp_channel}', True, True)
        
        self.set_archive_event('power_total_usage', True, True)
        self.set_change_event('power_total_usage', True, True)

        self.enable_monitoring(True)
        self.set_state(DevState.ON)

    def _callback_rfxtrx433(self, event: RFXtrx.SensorEvent) -> None:
        ''' Callback function for the RFXtrx library

        This is called whenever the hardware has detected a measurement event. 
        The event can be from anyone of the 3 temperature sensors or the power meter.
        The device and its data is decoded from the event and the appropriate
        Tango attributes are updated with events being pushed.
        '''
        with EnsureOmniThread():
            sensor_data: dict = event.values
            sensor_device: RFXtrx.RFXtrxDevice = event.device
            print(f"Callback event from instrument: {sensor_device}")
            print(f'  -> data: {sensor_data}')

            if sensor_device.type_string.startswith("ELEC"):
                # Energy meter measurement captured
                self.__meterreading = sensor_data["Total usage"]
                self.push_archive_event('power_total_usage', sensor_data["Total usage"])
                self.push_change_event('power_total_usage', sensor_data["Total usage"])

            elif sensor_device.type_string.startswith("Rubicson"):
                # Temperature monitor device
                temperature_channel = sensor_device.id_string.split(":")[1]
                new_temperature_reading = sensor_data["Temperature"]
                self.__temperatures[temperature_channel] = new_temperature_reading
                self.push_archive_event(f'temperature{temperature_channel}', new_temperature_reading)
                self.push_change_event(f'temperature{temperature_channel}', new_temperature_reading)
            else:
                # Unrecognised measurement channel... What now?
                pass
        # end-of EnsureOmniThread...

    # control (enable/disable) communication with the instrument
    @command(dtype_in=bool, dtype_out=DevVoid)
    def enable_monitoring(self, new_value: DevBoolean):
        if self.__rfxcomm is None and new_value:
            # Instanciating the low-level driver object. This in turn starts
            # a python thread which calls back into the callback function that
            # we register here.
            self.__rfxcomm = RFXtrx.Core(
                DEVICE_COMMS,
                self._callback_rfxtrx433,
                modes=["oregon", "rubicson"],
            )
        elif self.__rfxcomm is not None and not new_value:
            self.__rfxcomm.close_connection()
            self.__rfxcomm = None

    @attribute(archive_rel_change = 0.5, rel_change = 0.01)
    def power_total_usage(self) -> DevDouble:
        return self.__meterreading
    
    @attribute(archive_rel_change = 0.5, rel_change = 0.1)
    def temperature01(self) -> DevDouble:
        return self.__temperatures['01']
    
    @attribute(archive_rel_change = 0.5, rel_change = 0.1)
    def temperature02(self) -> DevDouble:
        return self.__temperatures['02']
    
    @attribute(archive_rel_change = 0.5, rel_change = 0.1)
    def temperature03(self) -> DevDouble:
        return self.__temperatures['03']

def main(args = None, **kwargs):
    return run((Rx433MHzSensors,), **kwargs)

if __name__ == '__main__':
    main()
