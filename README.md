# tango-433mhz-sensors

A Tango Device Server that can publish measurements from gadgets that transmit their data over the 433mhz band.

Requires some hardware: [RFXtrx433E USB controller](http://www.rfxcom.com/RFXtrx433E-USB-43392MHz-Transceiver/en)

Start the Tango Device Server like this:

```
python3 SensorDS.py test -v4 -nodb -port 45678 -dlist home/433mhzsensors/1
```
